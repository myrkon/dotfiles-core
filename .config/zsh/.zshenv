export HOME="/home/sonya"

source ~/.config/user-dirs.dirs

export XINITRC="$HOME/.config/X11/xinitrc"
export ERFILE="$HOME"/.cache/X11/xsession-error
export XAUTHORITY="$XDG_RUNTIME_DIR"/Xauthority

# chmod +t $XAUTHORITY
